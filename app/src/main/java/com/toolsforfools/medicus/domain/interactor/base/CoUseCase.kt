package com.toolsforfools.medicus.domain.interactor.base

import kotlinx.coroutines.flow.Flow

abstract class CoUseCase<T> {
    abstract suspend fun build(): Flow<T>
}