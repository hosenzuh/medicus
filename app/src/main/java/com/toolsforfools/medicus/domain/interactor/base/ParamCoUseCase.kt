package com.toolsforfools.medicus.domain.interactor.base

import kotlinx.coroutines.flow.Flow

abstract class ParamCoUseCase<PARAM, T> {

    abstract fun build(params: PARAM): Flow<T>
}