package com.toolsforfools.medicus.domain.model

data class Biomarker(
    val id: Int,
    val date: String?,
    val info: String?,
    val color: String?,
    val value: String?,
    val symbol: String?,
    val insight: String?,
    val category: String?,
)