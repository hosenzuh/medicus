package com.toolsforfools.medicus.domain.repository

import com.haroldadmin.cnradapter.NetworkResponse
import com.toolsforfools.medicus.data.remote.model.ErrorResponse
import com.toolsforfools.medicus.domain.model.Biomarker
import kotlinx.coroutines.flow.Flow

interface BiomarkerRepository {


    suspend fun getAllBiomarkers(): Flow<NetworkResponse<List<Biomarker>,ErrorResponse>>
}