package com.toolsforfools.medicus.domain.interactor.biomarker

import com.haroldadmin.cnradapter.NetworkResponse
import com.toolsforfools.medicus.data.remote.model.ErrorResponse
import com.toolsforfools.medicus.domain.interactor.base.CoUseCase
import com.toolsforfools.medicus.domain.model.Biomarker
import com.toolsforfools.medicus.domain.repository.BiomarkerRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllBiomarkersUseCase @Inject constructor(
    private val biomarkerRepository: BiomarkerRepository
) : CoUseCase<NetworkResponse<List<Biomarker>, ErrorResponse>>() {
    override suspend fun build(): Flow<NetworkResponse<List<Biomarker>, ErrorResponse>> =
        biomarkerRepository.getAllBiomarkers()
}