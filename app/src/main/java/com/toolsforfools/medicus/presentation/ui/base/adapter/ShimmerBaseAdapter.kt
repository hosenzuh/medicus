package com.toolsforfools.medicus.presentation.ui.base.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import com.toolsforfools.medicus.R
import com.toolsforfools.medicus.databinding.LayoutShimmerContainerBinding
import com.toolsforfools.medicus.utils.view.recursiveLoopChildren

abstract class ShimmerBaseAdapter<T> : BaseAdapter<T>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<T, out ViewDataBinding> {
        layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            DATA -> onCreateNormalViewHolder(parent)
            else -> {
                onCreateLoadingViewHolder(parent)
            }
        }
    }

    abstract fun provideLoadingLayoutId(): Int


    private fun onCreateLoadingViewHolder(parent: ViewGroup): BaseViewHolder<T, LayoutShimmerContainerBinding> {
        val binding = LayoutShimmerContainerBinding.inflate(
            layoutInflater,
            parent,
            false
        )
        val inflater =
            binding.root.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
        inflater?.inflate(provideLoadingLayoutId(), binding.shimmeringContainer)
        addGrayBackForItems(binding.shimmeringContainer)
        return LoadingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T, ViewDataBinding>, position: Int) {
        if (!isLoading)
            super.onBindViewHolder(holder, position)
    }

    inner class LoadingViewHolder(binding: LayoutShimmerContainerBinding) :
        BaseViewHolder<T, LayoutShimmerContainerBinding>(binding) {

        override fun onBind(position: Int, item: T) {
        }
    }

    private fun addGrayBackForItems(viewGroup: ViewGroup) {
        recursiveLoopChildren(viewGroup, startDoingDepth = 1, doOnView = {
            Log.e("TAG", "addGrayBackForItems: ${it.javaClass}", )
            it.setBackgroundColor(
                ContextCompat.getColor(
                    viewGroup.context,
                    R.color.gray
                )
            )
        })
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }


    override fun getItemCount(): Int = if (isLoading) 8 else list.size

    companion object {
        private const val DATA = 1
        private const val LOADING = 0
    }
}