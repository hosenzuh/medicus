package com.toolsforfools.medicus.presentation.ui.base.adapter

import androidx.databinding.ObservableBoolean

abstract class SelectableListAdapter<T> : BaseAdapter<T>() {

    val itemsState = ArrayList<ObservableBoolean>()
    override fun setData(data: List<T>?, notifyAll: Boolean) {
        super.setData(data, notifyAll)
        itemsState.clear()
        for (i in list)
            itemsState.add(ObservableBoolean(false))
    }

    override fun insertData(data: List<T>?) {
        super.insertData(data)
        if (data == null)
            return
        val newList = ArrayList<ObservableBoolean>(data.size)
        itemsState.addAll(newList)
    }

    override fun insertItemInIndex(item: T, index: Int) {
        super.insertItemInIndex(item, index)
        itemsState.add(index, ObservableBoolean(false))
    }

    override fun removeItem(item: T?) {
        val index = list.indexOf(item)
        super.removeItem(item)
        if (index == -1)
            return
        itemsState.removeAt(index)
    }

    override fun removeItem(index: Int) {
        super.removeItem(index)
        itemsState.removeAt(index)
    }

    override fun replaceItem(item: T, index: Int) {
        super.replaceItem(item, index)
        itemsState[index] = ObservableBoolean(false)
    }
}