package com.toolsforfools.medicus.presentation.ui.base.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.haroldadmin.cnradapter.NetworkResponse
import com.toolsforfools.medicus.data.remote.model.ErrorResponse

abstract class BaseViewModel : ViewModel() {

    val errorLiveData = MutableLiveData<String>()

    fun <T : Any> handleResponse(
        networkResponse: NetworkResponse<T, ErrorResponse>,
        holder: MutableLiveData<T>,
        doOnSuccess: () -> Unit = {
            holder.value = (networkResponse as NetworkResponse.Success).body
        },
        doOnServerError: () -> Unit = {
            errorLiveData.value =
                (networkResponse as NetworkResponse.ServerError<ErrorResponse>).body?.message
        },
        doOnError: () -> Unit = {
            errorLiveData.value =
                (networkResponse as NetworkResponse.Error).error.message
        }
    ) {
        when (networkResponse) {
            is NetworkResponse.Success -> doOnSuccess()
            is NetworkResponse.ServerError<ErrorResponse> -> doOnServerError()
            is NetworkResponse.Error -> doOnError()
        }
    }
}