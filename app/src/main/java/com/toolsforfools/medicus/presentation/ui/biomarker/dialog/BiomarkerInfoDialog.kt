package com.toolsforfools.medicus.presentation.ui.biomarker.dialog

import com.toolsforfools.medicus.R
import com.toolsforfools.medicus.databinding.DialogBiomarkerInfoBinding
import com.toolsforfools.medicus.presentation.ui.base.view.dialog.BaseDialogFragment
import com.toolsforfools.medicus.presentation.ui.biomarker.BiomarkersViewModel

class BiomarkerInfoDialog : BaseDialogFragment<BiomarkersViewModel, DialogBiomarkerInfoBinding>() {
    override fun getLayoutId(): Int = R.layout.dialog_biomarker_info

}