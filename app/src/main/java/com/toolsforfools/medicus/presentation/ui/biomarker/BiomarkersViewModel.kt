package com.toolsforfools.medicus.presentation.ui.biomarker

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.haroldadmin.cnradapter.NetworkResponse
import com.toolsforfools.medicus.domain.interactor.biomarker.GetAllBiomarkersUseCase
import com.toolsforfools.medicus.domain.model.Biomarker
import com.toolsforfools.medicus.presentation.ui.base.viewmodel.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BiomarkersViewModel @Inject constructor(
    private val getAllBiomarkersUseCase: GetAllBiomarkersUseCase
) : BaseViewModel() {

    init {
        getAll()
    }

    val selectedBiomarker = MutableLiveData<Biomarker>()
    val biomarkers = MutableLiveData<List<Biomarker>>()

    private fun getAll() {
        viewModelScope.launch {
            getAllBiomarkersUseCase.build().collect {
                handleResponse(
                    it,
                    biomarkers,
                    {
                        biomarkers.value =
                            (it as NetworkResponse.Success).body.filter { it.symbol != null }
                    })
            }
        }
    }
}