package com.toolsforfools.medicus.presentation.di

import com.toolsforfools.medicus.data.repository.BiomarkerRepositoryImpl
import com.toolsforfools.medicus.domain.repository.BiomarkerRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    fun provideBiomarkersRepository(biomarkerRepositoryImpl: BiomarkerRepositoryImpl): BiomarkerRepository
    
}