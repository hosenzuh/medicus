package com.toolsforfools.medicus.presentation.di

import com.toolsforfools.medicus.data.datasource.BiomarkersRemoteDataSource
import com.toolsforfools.medicus.data.datasource.BiomarkersRemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DataSourceModule {

    @Binds
    fun provideBiomarkersRemoteDataSource(biomarkersRemoteDataSourceImpl: BiomarkersRemoteDataSourceImpl):BiomarkersRemoteDataSource
}