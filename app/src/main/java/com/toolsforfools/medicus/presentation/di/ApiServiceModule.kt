package com.toolsforfools.medicus.presentation.di

import com.toolsforfools.medicus.data.remote.api.BiomarkerApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class ApiServiceModule {

    @Provides
    fun provideBiomarkersApiService(retrofit: Retrofit): BiomarkerApi =
        retrofit.create(BiomarkerApi::class.java)
}