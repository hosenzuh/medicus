package com.toolsforfools.medicus.presentation.ui.base.adapter

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.toolsforfools.medicus.R

open class BaseArrayAdapter<T>(context: Context, val getStringFunction: (T) -> String) :
    ArrayAdapter<T>(context, R.layout.support_simple_spinner_dropdown_item) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent)
        getItem(position)?.let { customizeTextView((view as TextView), it) }
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view = super.getView(position, convertView, parent)
        getItem(position)?.let { customizeTextView((view as TextView), it) }
        return view
    }

    open fun customizeTextView(textView: TextView, item: T) {
        textView.text = getStringFunction(item)
        textView.setTextColor(ContextCompat.getColor(context, R.color.black))
        textView.setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            context.resources.getDimensionPixelSize(R.dimen._13ssp).toFloat()
        )
    }

}