package com.toolsforfools.medicus.presentation.ui.base.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T> :
    RecyclerView.Adapter<BaseAdapter.BaseViewHolder<T, out ViewDataBinding>>() {

    lateinit var layoutInflater: LayoutInflater
    var isLoading = true
    var list = ArrayList<T>()

    /***
     * Add Data to the Existing List
     */
    open fun insertData(data: List<T>?) {
        if (data == null) return
        isLoading = false
        val index = list.size
        list.addAll(data)
        if (index == 0) notifyDataSetChanged()
        else notifyItemInserted(index)
    }

    /***
     * Replace Data with the new one
     */

    open fun setData(data: List<T>?, notifyAll: Boolean = true) {
        if (data == null) return
        isLoading = false
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    open fun insertItemInIndex(item: T, index: Int) {
        if (index < 0) return
        if (index < list.size) {
            list.add(index, item)
        } else list.add(item)
        notifyDataSetChanged()
    }

    open fun removeItem(item: T?) {
        if (item == null) return
        val index: Int = list.indexOf(item)
        if (index == -1) return  // not found
        list.removeAt(index)
        notifyItemRemoved(index)
        notifyItemRangeChanged(index, list.size - 1)
    }

    open fun removeItem(index: Int) {
        if (index < 0 || index >= list.size) return
        list.removeAt(index)
        notifyItemRemoved(index)
        notifyItemRangeChanged(index, list.size - 1)
    }

    open fun replaceItem(item: T, index: Int) {
        if (index < 0 || index >= list.size) return
        removeItem(index)
        insertItemInIndex(item, index)
    }


    open fun addLoadListener(function: () -> Unit) {
        if (!isLoading)
            function.invoke()
    }

    override fun getItemViewType(position: Int): Int =
        if (isLoading) LOADING else DATA


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<T, ViewDataBinding> {
        layoutInflater = LayoutInflater.from(parent.context)
        return onCreateNormalViewHolder(parent)
    }

    override fun getItemCount() = list.size

    abstract fun onCreateNormalViewHolder(parent: ViewGroup): BaseViewHolder<T, ViewDataBinding>

    override fun onBindViewHolder(holder: BaseViewHolder<T, ViewDataBinding>, position: Int) {
        holder.onBind(
            position,
            list[position]
        )
    }

    abstract class BaseViewHolder<T, out VB : ViewDataBinding>(open val binding: VB) :
        RecyclerView.ViewHolder(binding.root) {
        abstract fun onBind(position: Int, item: T)
    }

    companion object {
        private const val DATA = 1
        private const val LOADING = 0
    }

}