package com.toolsforfools.medicus.presentation.ui.biomarker.adpater

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.toolsforfools.medicus.R
import com.toolsforfools.medicus.databinding.ListItemBiomarkerBinding
import com.toolsforfools.medicus.domain.model.Biomarker
import com.toolsforfools.medicus.presentation.ui.base.adapter.ShimmerBaseAdapter

class BiomarkersListAdapter(
    private val onItemClicked: (Biomarker) -> Unit
) :
    ShimmerBaseAdapter<Biomarker>() {
    override fun onCreateNormalViewHolder(parent: ViewGroup): BaseViewHolder<Biomarker, ViewDataBinding> =
        BiomarkerViewHolder(
            ListItemBiomarkerBinding.inflate(layoutInflater, parent, false)
        )

    inner class BiomarkerViewHolder(binding: ListItemBiomarkerBinding) :
        BaseViewHolder<Biomarker, ListItemBiomarkerBinding>(
            binding
        ) {
        override fun onBind(position: Int, item: Biomarker) {
            binding.biomarker = item
            binding.root.setOnClickListener { onItemClicked(item) }
        }
    }

    override fun provideLoadingLayoutId(): Int = R.layout.list_item_biomarker
}