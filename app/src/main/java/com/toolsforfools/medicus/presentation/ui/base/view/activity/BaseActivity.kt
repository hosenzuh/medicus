package com.toolsforfools.medicus.presentation.ui.base.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.toolsforfools.medicus.BR
import com.toolsforfools.medicus.presentation.ui.base.viewmodel.BaseViewModel
import com.toolsforfools.medicus.utils.Progressbar
import com.toolsforfools.medicus.utils.toast
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<VM : BaseViewModel, B : ViewDataBinding> : AppCompatActivity() {

    lateinit var binding: B
    lateinit var viewModel: VM

    public var progressbar: Progressbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        viewModel = ViewModelProvider(getViewModelOwner()).get(getViewModelClass())
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        putViewModelInBinding()
        setOnBackButtonClickedListener()


        progressbar = Progressbar.getInstance(this)

        observeErrorLiveData()
    }

//    fun doAfterCheckPermission(
//        function: () -> Unit,
//        vararg permissions: String,
//        requestCode: Int = 300
//    ) {
//        if (EasyPermissions.hasPermissions(this, *permissions))
//            function()
//        else
//            EasyPermissions.requestPermissions(this, "Give permissions", requestCode, *permissions)
//    }

    private fun setOnBackButtonClickedListener() {
        try {
//            binding.root.findViewById<View>(R.id.back_button).setOnClickListener {
//                onBackPressed()
//            }
        } catch (e: Exception) {
        }
    }

    private fun observeErrorLiveData() {
        viewModel.errorLiveData.observe(this) {
            toast(it)
        }
    }

    private fun putViewModelInBinding() {
        try {
            binding.setVariable(getViewModelId(), viewModel)
        } catch (e: Exception) {
        }
    }

    open fun getViewModelId(): Int = BR.viewModel


    private fun getViewModelClass(): Class<VM> {
        val type = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0]
        return type as Class<VM>
    }

    open fun getViewModelOwner(): ViewModelStoreOwner = this

    abstract fun getLayoutId(): Int


    fun showProgress() {
        progressbar?.show()
    }

    fun hideProgress() {
        progressbar?.hide()
    }

    companion object {
        /**
         * start activity with the pairs provided
         * */
        fun <T> getStarterIntent(
            context: Context,
            clazz: Class<T>,
            vararg data: Pair<String, Any?>
        ) {
            val bundle = bundleOf(*data)
            val intent = Intent(context, clazz)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }
}
