package com.toolsforfools.medicus.presentation.ui.biomarker.fragment

import android.os.Bundle
import android.view.View
import com.toolsforfools.medicus.R
import com.toolsforfools.medicus.databinding.FragmentBiomarkerDetailsBinding
import com.toolsforfools.medicus.presentation.ui.base.view.fragment.BaseFragment
import com.toolsforfools.medicus.presentation.ui.biomarker.BiomarkersViewModel
import com.toolsforfools.medicus.presentation.ui.biomarker.dialog.BiomarkerInfoDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BiomarkerDetailsFragment :
    BaseFragment<BiomarkersViewModel, FragmentBiomarkerDetailsBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_biomarker_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
    }

    private fun setupListeners() {
        binding.infoIcon.setOnClickListener {
            BiomarkerInfoDialog().show(childFragmentManager, "Info")
        }
    }
}