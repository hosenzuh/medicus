package com.toolsforfools.medicus.presentation.ui.biomarker

import com.toolsforfools.medicus.R
import com.toolsforfools.medicus.databinding.ActivityBiomarkersBinding
import com.toolsforfools.medicus.presentation.ui.base.view.activity.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BiomarkersActivity : BaseActivity<BiomarkersViewModel, ActivityBiomarkersBinding>() {
    override fun getLayoutId(): Int = R.layout.activity_biomarkers
}