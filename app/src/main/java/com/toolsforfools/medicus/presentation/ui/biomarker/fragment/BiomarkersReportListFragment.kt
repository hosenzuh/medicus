package com.toolsforfools.medicus.presentation.ui.biomarker.fragment

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.toolsforfools.medicus.R
import com.toolsforfools.medicus.databinding.FragmentBiomarkersReportListBinding
import com.toolsforfools.medicus.presentation.ui.base.view.fragment.BaseFragment
import com.toolsforfools.medicus.presentation.ui.biomarker.BiomarkersViewModel
import com.toolsforfools.medicus.presentation.ui.biomarker.adpater.BiomarkersListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BiomarkersReportListFragment :
    BaseFragment<BiomarkersViewModel, FragmentBiomarkersReportListBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_biomarkers_report_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList()
    }

    private fun setupList() {
        binding.biomarkersList.adapter =
            BiomarkersListAdapter {
                viewModel.selectedBiomarker.value = it
                goToDetails()
            }
    }

    private fun goToDetails() {
        findNavController().navigate(R.id.action_biomarkersReportListFragment_to_biomarkerDetailsFragment)
    }
}