package com.toolsforfools.medicus.presentation.ui.base.view.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.toolsforfools.medicus.BR
import com.toolsforfools.medicus.presentation.ui.base.view.activity.BaseActivity
import com.toolsforfools.medicus.presentation.ui.base.viewmodel.BaseViewModel
import com.toolsforfools.medicus.utils.Progressbar
import java.lang.reflect.ParameterizedType

abstract class BaseDialogFragment<VM : BaseViewModel, B : ViewDataBinding> : DialogFragment() {

    lateinit var binding: B
    open lateinit var viewModel: VM
    protected lateinit var parent: BaseActivity<*, *>

    var progressbar: Progressbar? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(getViewModelOwner()).get(getViewModelClass())
        progressbar = Progressbar.getInstance(requireActivity())
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        parent = context as BaseActivity<*, *>
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        putViewModelInBinding()
        setOnBackButtonClickedListener()
    }

    private fun setOnBackButtonClickedListener() {
        try {
//            binding.root.findViewById<View>(R.id.back_button).setOnClickListener {
//                requireActivity().onBackPressed()
//            }
        } catch (e: Exception) {
        }
    }


    private fun putViewModelInBinding() {
        try {
            binding.setVariable(getViewModelId(), viewModel)
        } catch (e: Exception) {
        }
    }

    open fun getViewModelId(): Int = BR.viewModel


    private fun getViewModelClass(): Class<VM> {
        val type = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0]
        return type as Class<VM>
    }

    /**
     * override it to change viewModel owner
     */
    open fun getViewModelOwner(): ViewModelStoreOwner = requireActivity()

    abstract fun getLayoutId(): Int

    /**
     * For progress bar handling
     * */
    fun showProgress() {
        progressbar?.show()
    }

    fun hideProgress() {
        progressbar?.hide()
    }

//    fun doAfterCheckPermission(
//        function: () -> Unit,
//        vararg permissions: String,
//        requestCode: Int = 300
//    ) {
//        if (EasyPermissions.hasPermissions(requireContext(), *permissions))
//            function()
//        else
//            EasyPermissions.requestPermissions(this, "Give permissions", requestCode, *permissions)
//    }

//    fun <T> handleTagAddingListener(
//        tagContainer: LayoutTagContainerBinding,
//        allCategories: MutableLiveData<List<T>>,
//        selectedCategories: MutableLiveData<HashSet<T>>,
//        getStringFunction: (T) -> String
//    ) {
//        tagContainer.tagsAutoCompleteTextView.setOnAddTagListener { name ->
//            if (selectedCategories.value?.map { getStringFunction(it) }?.contains(name)
//                    ?.not() == true
//            ) {
//                val category =
//                    allCategories.value?.find { getStringFunction(it) == name }
//                selectedCategories.value.apply { category?.let { this?.add(it) } }
//                selectedCategories.value = selectedCategories.value
//                tagContainer.tagsChipGroup.addChip(category, name) {
//                    selectedCategories.value?.remove(it)
//                    selectedCategories.value = selectedCategories.value
//                }
//            }
//            tagContainer.tagsAutoCompleteTextView.text = null
//        }
//    }
//
//    fun <T> handleTagWithSearchAddingListener(
//        tagContainer: LayoutTagContainerBinding,
//        allCategories: MutableLiveData<List<T>>,
//        selectedCategories: MutableLiveData<HashSet<T>>,
//        onTextChanged: (String) -> Unit,
//        getStringFunction: (T) -> String,
//    ) {
//        tagContainer.tagsAutoCompleteTextView.setOnAddTagListener { name ->
//            if (selectedCategories.value?.map { getStringFunction(it) }?.contains(name)
//                    ?.not() == true
//            ) {
//                val category =
//                    allCategories.value?.find { getStringFunction(it) == name }
//                selectedCategories.value.apply { category?.let { this?.add(it) } }
//                selectedCategories.value = selectedCategories.value
//                tagContainer.tagsChipGroup.addChip(category, name) {
//                    selectedCategories.value?.remove(it)
//                    selectedCategories.value = selectedCategories.value
//                }
//            }
//            tagContainer.tagsAutoCompleteTextView.text = null
//        }
//        tagContainer.tagsAutoCompleteTextView.doOnTextChanged { text, start, before, count ->
//            if (text != null && !text.isEmpty())
//                onTextChanged(text.toString())
//        }
//        allCategories.observe(viewLifecycleOwner) {
//            tagContainer.tagsAutoCompleteTextView.dismissDropDown()
//            tagContainer.tagsAutoCompleteTextView.showDropDown()
//        }
//    }
//
//    fun <T> handleSpinnerAdapter(
//        spinner: Spinner,
//        holder: MutableLiveData<T>,
//        getStringFunction: (T) -> String
//    ) {
//        val adapter = BaseArrayAdapter(requireContext(), getStringFunction)
//        spinner.adapter = adapter
//        spinner.onItemSelectedListener = object : OnSpinnerItemSelectedListener() {
//            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                val item = (p0?.getItemAtPosition(p2) as T)
//                holder.value = item
//                (p1 as TextView).text = getStringFunction(item)
//                adapter.notifyDataSetChanged()
//            }
//        }
//    }
}