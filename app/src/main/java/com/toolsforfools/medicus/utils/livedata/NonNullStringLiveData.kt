package com.toolsforfools.medicus.utils.livedata

class NonNullStringLiveData(value: String = "") :
    CheckableLiveData<String>({ s -> s.isNotEmpty() }) {
    init {
        this.value = value
    }
}