package com.toolsforfools.medicus.utils.livedata

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

class MultiMediatorLiveData<T>(
    val onChanged: () -> T
) : MediatorLiveData<T>() {

    fun addMultiSource(vararg liveData: MutableLiveData<out Any>) {
        for (item in liveData)
            addSource(item) {
                value = onChanged()
            }
    }
}