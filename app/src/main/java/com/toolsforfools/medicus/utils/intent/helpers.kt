package com.toolsforfools.medicus.utils.intent

import android.content.Context
import android.telephony.SmsManager
import android.widget.Toast

fun Context.sendMessage(phoneNumber: String): Boolean {
    try {
        val smsManager: SmsManager = SmsManager.getDefault()
        smsManager.sendTextMessage(
            phoneNumber,
            null,
            "نود إعلامكم بأن إبنكم تغيب اليوم عن الحضور للمسجد",
            null,
            null
        )
        return true
    } catch (ex: Exception) {
        Toast.makeText(
            applicationContext, ex.message.toString(),
            Toast.LENGTH_LONG
        ).show()
        ex.printStackTrace()
        return false
    }
}