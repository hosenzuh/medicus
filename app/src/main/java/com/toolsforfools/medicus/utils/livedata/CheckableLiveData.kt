package com.toolsforfools.medicus.utils.livedata

import androidx.lifecycle.MutableLiveData

open class CheckableLiveData<T>(val validateCheck: ((T) -> Boolean)? = null) :
    MutableLiveData<T>() {


    fun check(): Boolean = value?.let { validateCheck?.invoke(it) } ?: true
}