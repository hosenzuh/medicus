package com.toolsforfools.medicus.utils

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.toolsforfools.medicus.R

class Progressbar(activity: Activity) {
    private var mDialog: Dialog = Dialog(activity)

    init {
        mDialog.setContentView(R.layout.layout_progressbar)
        mDialog.setCancelable(false)
        mDialog.setCanceledOnTouchOutside(false)
        mDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    companion object {
        fun getInstance(mActivity: Activity): Progressbar {
            return Progressbar(mActivity)
        }
    }

    fun show() {
        mDialog.show()
    }

    fun hide() {
        mDialog.dismiss()
    }
}