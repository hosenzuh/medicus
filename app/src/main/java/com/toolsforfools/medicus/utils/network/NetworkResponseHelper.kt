package com.toolsforfools.medicus.utils.network

import com.haroldadmin.cnradapter.NetworkResponse

fun <T : Any, SUCCESS : Any, ERROR : Any> NetworkResponse<SUCCESS, ERROR>.map(mapper: (SUCCESS) -> T):
        NetworkResponse<T, ERROR> {
    when (this) {
        is NetworkResponse.Success<SUCCESS> -> {
            return NetworkResponse.Success(mapper(this.body), this.headers, this.code)
        }
        is NetworkResponse.ServerError -> return NetworkResponse.ServerError(
            this.body,
            this.code,
            this.headers
        )
        is NetworkResponse.NetworkError -> return NetworkResponse.NetworkError(this.error)
        is NetworkResponse.UnknownError -> return NetworkResponse.UnknownError(
            this.error,
            code,
            this.headers
        )
    }
}