package com.toolsforfools.medicus.utils.bindingadapter

import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.toolsforfools.medicus.presentation.ui.base.adapter.BaseAdapter

@BindingAdapter("android:loadData", "android:notify", requireAll = false)
fun <T> loadDataList(
    recyclerView: RecyclerView,
    data: List<T>?,
    notifyAll: Boolean = true
) {
    if (recyclerView.adapter is BaseAdapter<*>) {
        (recyclerView.adapter as BaseAdapter<T>).setData(data, notifyAll)
    }
}

@BindingAdapter("android:loadData", "android:notify", requireAll = false)
fun <T> loadDataList(
    viewPager2: ViewPager2, data: List<T>?,
    notifyAll: Boolean = true
) {
    if (viewPager2.adapter is BaseAdapter<*>) {
        (viewPager2.adapter as BaseAdapter<T>).setData(data, notifyAll)
    }
}

@BindingAdapter("android:loadData")
fun <T> loadSpinnerData(spinner: Spinner, data: List<T>?) {
    val adapter = spinner.adapter
    if (adapter is ArrayAdapter<*>) {
        if (data != null) {
            (adapter as ArrayAdapter<T>).addAll(data)
        }
    }
}


//@BindingAdapter("android:loadPagingData")
//fun <T> loadPagingDataList(
//    recyclerView: RecyclerView,
//    result: Result<BaseModel<PaginationModel<T>>>?
//) {
//    if (result == null) return
//    if (recyclerView.adapter is BaseAdapter<*>) {
//        addDataToList(
//            recyclerView.adapter,
//            result
//        )
//    }
//}

