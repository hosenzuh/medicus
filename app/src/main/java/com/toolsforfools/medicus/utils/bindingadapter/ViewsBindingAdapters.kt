package com.toolsforfools.medicus.utils.bindingadapter

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.graphics.Color
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.annotation.FontRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import com.google.android.material.button.MaterialButton
import com.google.android.material.tabs.TabLayout
import com.toolsforfools.medicus.R
import com.toolsforfools.medicus.utils.edittext.InputFilterMinMax
import java.util.*
import kotlin.math.roundToInt

@BindingAdapter("android:fontFamily")
fun setFont(button: MaterialButton, @FontRes fontId: Int) {
    button.typeface = ResourcesCompat.getFont(button.context, fontId)
}

@BindingAdapter("android:textColor")
fun setTextColor(textView: TextView, colorHex: String?) {
    if (colorHex == null) return
    try {
        textView.setTextColor(Color.parseColor(colorHex))
    } catch (e: Exception){

    }
}

//@BindingAdapter("app:cornerRadius")
//fun setCornerRadius(button: MaterialButton, radius: Float) {
//    button.cornerRadius = radius.toInt()
//}

@BindingAdapter("android:existence")
fun setExistence(view: View, exist: Boolean) {
    view.visibility = if (exist) View.VISIBLE else View.GONE
}

@BindingAdapter("android:visibility")
fun setVisibility(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.INVISIBLE
}


@BindingAdapter("android:background")
fun loadBackgroundImage(
    view: View,
    @DrawableRes drawableRes: Int?
) {
    view.background = drawableRes?.let { ContextCompat.getDrawable(view.context, drawableRes) }
}

@BindingAdapter("android:src")
fun loadImage(
    imageView: ImageView?,
    url: String?
) {
//    imageView?.load(url)
}

@BindingAdapter("android:text")
fun integerText(textView: TextView, number: Int) {
    textView.text = number.toString()
}

@BindingAdapter("android:text")
fun doubleText(textView: TextView, number: Double) {
    textView.text = number.toString()
}

// AutoCompleteText adapter
@BindingAdapter("android:hints")
fun addHints(autoCompleteTextView: AutoCompleteTextView, hints: List<String>?) {
    if (hints == null) return
    val adapter = ArrayAdapter(
        autoCompleteTextView.context,
        R.layout.support_simple_spinner_dropdown_item,
        hints
    )

    autoCompleteTextView.setAdapter(adapter)
    autoCompleteTextView.setOnFocusChangeListener { view, _ ->
        if (view.equals(autoCompleteTextView))
            autoCompleteTextView.showDropDown()
    }
    autoCompleteTextView.setOnClickListener {
        autoCompleteTextView.showDropDown()
    }
}

@BindingAdapter("isBold")
fun setBold(view: TextView, isBold: Boolean) {
    view.setTypeface(null, if (isBold) Typeface.BOLD else Typeface.NORMAL)
}


@BindingAdapter("hint")
fun setHint(textView: TextView, stringId: Int) {
    if (textView.text.toString() == "") {
        val text = textView.context.getString(stringId)
        textView.text = text
    }
}

@BindingAdapter("android:isIndicatorVisible")
fun setIndicatorVisibility(tabLayout: TabLayout, isVisible: Boolean) {
    if (!isVisible)
        tabLayout.setSelectedTabIndicatorColor(
            ContextCompat.getColor(
                tabLayout.context,
                android.R.color.transparent
            )
        )
}

@BindingAdapter("android:layout_height")
fun setHeight(view: View, float: Float) {
    view.layoutParams.height =
        if (float.equals(-2.0)) ViewGroup.LayoutParams.WRAP_CONTENT else float.roundToInt()
}

// to put the button clickable and enabled
@BindingAdapter("available")
fun setButtonAvailable(button: Button, boolean: Boolean) {
    button.isEnabled = boolean
    button.isClickable = boolean
}

// set min value for numbered edit text
@BindingAdapter("minValue", "maxValue", requireAll = false)
fun setMinValue(editText: EditText, min: Int?, max: Int?) {
    if (min == null && max == null) return
    editText.filters = arrayOf(InputFilterMinMax(min ?: Int.MIN_VALUE, max ?: Int.MAX_VALUE))
}

@SuppressLint("SetTextI18n")
@BindingAdapter("calendar")
fun bindCalendarWithTextView(imageButton: ImageButton, textView: TextView) {

    val calendar = Calendar.getInstance(TimeZone.getDefault())
    val onDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, month, day ->
            val date = "${String.format(Locale.ENGLISH, "%04d", year)}-${
                String.format(
                    Locale.ENGLISH,
                    "%02d",
                    month
                )
            }-${String.format(Locale.ENGLISH, "%02d", day)}"
            textView.text = date
        }

    imageButton.setOnClickListener {
        DatePickerDialog(
            imageButton.context, onDateSetListener, calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
        ).show()
    }
}

@BindingAdapter("isButtonAvailable")
fun isButtonAvailable(button: Button, boolean: Boolean) {
    button.isClickable = boolean
    button.isEnabled = boolean
}