package com.toolsforfools.medicus.utils

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Fragment.toast(text: String?) {
    if (text.isNullOrEmpty()) return
    Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
}

fun Context.toast(text: String?) {
    if (text.isNullOrEmpty()) return
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}