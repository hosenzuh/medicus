package com.toolsforfools.medicus.utils.view

import android.graphics.Color

fun parseColorWithCheck(colorHex: String?): Int {
    if (colorHex.isNullOrEmpty()) return Color.TRANSPARENT
    return try {
        Color.parseColor(colorHex)
    } catch (e: Exception) {
        Color.TRANSPARENT
    }
}