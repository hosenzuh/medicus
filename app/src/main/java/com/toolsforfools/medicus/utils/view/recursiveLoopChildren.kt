package com.toolsforfools.medicus.utils.view

import android.util.Log
import android.view.View
import android.view.ViewGroup

fun recursiveLoopChildren(
    parent: ViewGroup?,
    doOnViewGroup: ((ViewGroup) -> Unit)? = null,
    doOnView: ((View) -> Unit)? = null,
    depth: Int = 0,
    startDoingDepth: Int = 0
) {
    for (i in 0 until parent!!.childCount) {
        val child: View? = parent.getChildAt(i)
        if (child is ViewGroup) {
            recursiveLoopChildren(
                child as ViewGroup?,
                doOnViewGroup,
                doOnView,
                depth + 1,
                startDoingDepth
            )
            if (depth >= startDoingDepth)
                doOnViewGroup?.invoke(child)
        } else {
            if (child != null) {
                if (depth >= startDoingDepth) {
                    doOnView?.invoke(child)
                }
            }
        }
    }
}