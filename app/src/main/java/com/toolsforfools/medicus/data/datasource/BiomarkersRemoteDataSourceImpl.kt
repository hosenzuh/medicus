package com.toolsforfools.medicus.data.datasource

import com.haroldadmin.cnradapter.NetworkResponse
import com.toolsforfools.medicus.data.model.BiomarkerModel
import com.toolsforfools.medicus.data.remote.api.BiomarkerApi
import com.toolsforfools.medicus.data.remote.model.ErrorResponse
import javax.inject.Inject

class BiomarkersRemoteDataSourceImpl @Inject constructor(
    private val biomarkerApi: BiomarkerApi
) : BiomarkersRemoteDataSource {
    override suspend fun getAllBiomarkers(): NetworkResponse<List<BiomarkerModel>, ErrorResponse> =
        biomarkerApi.getAllBiomarkers()
}