package com.toolsforfools.medicus.data.remote.constants

object ApiConstants {

    const val BASE_URL = "https://retoolapi.dev/hZZ5j8/"

    //Biomarkers
    const val GET_ALL_BIOMARKERS = "biomarkers"
}