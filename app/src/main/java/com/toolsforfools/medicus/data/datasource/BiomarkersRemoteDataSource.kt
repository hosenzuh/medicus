package com.toolsforfools.medicus.data.datasource

import com.haroldadmin.cnradapter.NetworkResponse
import com.toolsforfools.medicus.data.model.BiomarkerModel
import com.toolsforfools.medicus.data.remote.model.ErrorResponse

interface BiomarkersRemoteDataSource {

    suspend fun getAllBiomarkers(): NetworkResponse<List<BiomarkerModel>, ErrorResponse>
}