package com.toolsforfools.medicus.data.model

import com.google.gson.annotations.SerializedName

data class BiomarkerModel(
    @SerializedName("id") val id: Int,
    @SerializedName("date") val date: String?,
    @SerializedName("info") val info: String?,
    @SerializedName("color") val color: String?,
    @SerializedName("value") val value: String?,
    @SerializedName("symbol") val symbol: String?,
    @SerializedName("insight") val insight: String?,
    @SerializedName("category") val category: String?,
)