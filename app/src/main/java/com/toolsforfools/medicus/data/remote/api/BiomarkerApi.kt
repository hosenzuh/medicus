package com.toolsforfools.medicus.data.remote.api

import com.haroldadmin.cnradapter.NetworkResponse
import com.toolsforfools.medicus.data.model.BiomarkerModel
import com.toolsforfools.medicus.data.remote.constants.ApiConstants.GET_ALL_BIOMARKERS
import com.toolsforfools.medicus.data.remote.model.ErrorResponse
import retrofit2.Response
import retrofit2.http.GET

interface BiomarkerApi {

    @GET(GET_ALL_BIOMARKERS)
    suspend fun getAllBiomarkers(): NetworkResponse<List<BiomarkerModel>, ErrorResponse>

}