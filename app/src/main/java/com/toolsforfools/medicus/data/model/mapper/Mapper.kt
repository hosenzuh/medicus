package com.toolsforfools.medicus.data.model.mapper

abstract class Mapper<TO, FROM> {

    abstract fun map(model: FROM): TO

    fun map(models: List<FROM>): List<TO> {
        return List(models.size) { map(models[it]) }
    }

    /**
     * Implement it only if necessary
     */
    open fun unmap(model: TO): FROM? {
        return null
    }

    fun unmap(models: List<TO>): List<FROM?> = List(models.size) { unmap(models[it]) }

}