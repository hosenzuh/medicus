package com.toolsforfools.medicus.data.repository

import com.haroldadmin.cnradapter.NetworkResponse
import com.toolsforfools.medicus.data.datasource.BiomarkersRemoteDataSource
import com.toolsforfools.medicus.data.model.mapper.BiomarkerMapper
import com.toolsforfools.medicus.data.remote.model.ErrorResponse
import com.toolsforfools.medicus.domain.model.Biomarker
import com.toolsforfools.medicus.domain.repository.BiomarkerRepository
import com.toolsforfools.medicus.utils.network.map
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class BiomarkerRepositoryImpl @Inject constructor(
    private val biomarkersRemoteDataSource: BiomarkersRemoteDataSource,
    private val biomarkerMapper: BiomarkerMapper
) :
    BiomarkerRepository {
    override suspend fun getAllBiomarkers(): Flow<NetworkResponse<List<Biomarker>, ErrorResponse>> =
        flow {
            emit(biomarkersRemoteDataSource.getAllBiomarkers().map { biomarkerMapper.map(it) })
        }
}