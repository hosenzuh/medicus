package com.toolsforfools.medicus.data.model.mapper

import com.toolsforfools.medicus.data.model.BiomarkerModel
import com.toolsforfools.medicus.domain.model.Biomarker
import javax.inject.Inject

class BiomarkerMapper @Inject constructor() : Mapper<Biomarker, BiomarkerModel>() {

    override fun map(model: BiomarkerModel): Biomarker = Biomarker(
        model.id,
        model.date,
        model.info,
        model.color,
        model.value,
        model.symbol,
        model.insight,
        model.category
    )
}